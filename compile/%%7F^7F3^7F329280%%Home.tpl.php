<?php /* Smarty version 2.6.22, created on 2017-05-10 22:31:24
         compiled from Home.tpl */ ?>
	<!DOCTYPE html>
	<html lang="en">
<!-- /*MODAL*/ -->

<div class="container-fluid">
<hr>
<!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-default"  data-toggle="modal" data-target="#myModal">Add new task</button>
<hr>
  <!-- Modal -->
  
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form name="FormForModal" method="post" action="TaskCreationAction.php">
 <input class="form-control" type="text" name="TaskSummary" id="TaskSummary">       
	   </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
		</form>
      </div>
    </div>
  </div>
</div>
  
  
  

<!-- Modal -->
<!-- /*TABLE*/ -->
<div class="container-fluid">
<table class="table table-hover">
<thead></thead>
<tr>
<th>Task Number</th>
<th>Task Summary</th>
<th>Planned For</th>
<th>Status</th>
</tr>
<tbody>
    <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
      <tr>
        <td><a href="TaskDetail.php?TaskId=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
"><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
</a></td>
        <td><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskSummary']; ?>
</td>
		 <td><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['PlannedFor']; ?>
</td>
        <td><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['Status']; ?>
</td>
       </tr>
       <?php endfor; endif; ?>
      </tbody>
</table>

</div>


</body>
</html>



