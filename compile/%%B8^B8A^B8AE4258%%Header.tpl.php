<?php /* Smarty version 2.6.22, created on 2017-05-10 22:40:44
         compiled from Header.tpl */ ?>
	<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Task Manager</title>
     <link rel="icon" type="image/png" href="images/favicon.png"  />
   <!-- Custom CSS -->
  <link rel="stylesheet" href="css/style.css" type="text/css">
    <!-- Required meta tags -->
<meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    
  </head>
  <body>


    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	

 <!-- Custom JS -->
    <script src="js/myjs.js"></script>
	
 
    <nav class="navbar navbar-light" style="background-color: #f2f2f2;">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="Home.php">Task Manager</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
        <a class="nav-item nav-link" href="Daily.php">Daily</a>
        <a class="nav-item nav-link" href="Weekly.php">Weekly</a>
        <a class="nav-item nav-link" href="Monthly.php">Monthly</a>
		<a class="nav-item nav-link" href="Future.php">Future</a>
		<a class="nav-item nav-link" href="AllOpenTasks.php">All Open Tasks</a>
        <a class="nav-item nav-link" href="Archieve.php">Closed Tasks</a>
      <a class="nav-item nav-link" href="Hold.php">Hold</a>
      <a class="nav-item nav-link" href="Logout.php">Exit</a>
    
    </div>
  </div>
</nav>