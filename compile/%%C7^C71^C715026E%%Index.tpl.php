<?php /* Smarty version 2.6.22, created on 2017-05-07 21:47:12
         compiled from Index.tpl */ ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Task Manager</title>
   <!-- Custom CSS -->
   <link rel="icon" type="image/png" href="images/favicon.png"  />
  <link rel="stylesheet" href="css/styles.css" type="text/css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    
  </head>
  <body>


   
 

 <!-- Custom JS -->
    <script src="js/myjs.js"></script>
	
 
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="index.php">Task Manager</a>

</nav>