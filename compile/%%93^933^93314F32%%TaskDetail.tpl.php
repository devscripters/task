<?php /* Smarty version 2.6.22, created on 2017-05-08 21:58:08
         compiled from TaskDetail.tpl */ ?>

<!DOCTYPE html>
<html lang="en">
<!-- /*ICONS*/ -->
<link rel='stylesheet' href='css/bootstrap.min.css' />
<!-- /*ICONS*/ -->
<!-- /*************************************Heading************************************/ -->
<hr>
<!-- /************************************Back button************************************/ -->
<div class="container-fluid">
<a href="Home.php" class="btn btn-secondary" role="button" aria-pressed="true">
<span class="glyphicon glyphicon-arrow-left"></span>
</a>
</div>
<!-- /*Back button*/ -->
<div class="container-fluid">
<div class="row">
<div class="col-lg-10">
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<form name="EditSummary" id="EditSummary" method="post" action="EditSummaryAction.php" enctype="multipart/form-data">
<input type="hidden" name="TaskId" id ="TaskId" value=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
>
<div class="container-fluid-fluid" id="header">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv2" aria-expanded="false" aria-controls="IdHiddenDiv2" >
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<p>
<textarea class="form-control" rows="1" name="Summary" id="Summary" ><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskSummary']; ?>
 </textarea>
<?php endfor; endif; ?>
 </p>
<div class="collapse" id="IdHiddenDiv2">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</form>
</div>
</div>
</div>
</div>
<!-- /************************************Options************************************/ -->
<div class="col-lg-2">
<div class="dropdown">
<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-option-horizontal"></span>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<a class="dropdown-item" href="CreateSubTaskAction.php?TaskId=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
">Create sub-task</a>
<?php endfor; endif; ?>
<a class="dropdown-item" href="#">Another action</a>
<a class="dropdown-item" href="#">Something else here</a>
</div>
</div>
</div>
<!-- /*Options*/ -->
</div>
<!-- /*Heading*/ -->
<!-- /************************************Details************************************/ -->
<div class="row">
<div class="col-lg-8">
<!-- Line -->
<h6>Details</h6>
<!-- Line -->
<form name="edit" id="edit "method="post" action="EditStatusAction.php" enctype="multipart/form-data">
<!-- /*Edit Button*/ -->
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv3" aria-expanded="false" aria-controls="IdHiddenDiv3">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<!-- /*Edit Button*/ -->
<!-- /*Hidden*/ -->
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<input type="hidden" name="TaskId" id ="TaskId" value=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
 >
<?php endfor; endif; ?>
<!-- /*Hidden*/ -->
<!-- /*Status*/ -->
<div class="form-group">
<label for="StatusDdl" class="mr-sm-2">Status</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="StatusDdl" name="StatusDdl">
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<option selected><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['Status']; ?>
</option>
<?php endfor; endif; ?>
<option>OPEN</option>
<option>CLOSE</option>
<option>HOLD</option>
<option>FIXED</option> 
<option>IN PROGRESS</option>  
</select>
</div>
<!-- /*Status*/ -->
<!-- /*PlannedFor*/ -->
<div class="form-group">
<label for="PlannedForLabel" class="mr-sm-2">Planned For</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="PlannedDdl" name="PlannedDdl">
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<option selected><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['PlannedFor']; ?>
</option>
<?php endfor; endif; ?>
<option>Today</option>
<option>Daily</option>
<option>Weekly</option>
<option>Monthly</option>
<option>Future</option>
</select>
</div>
<!-- /*PlannedFor*/ -->
<!-- /*Submit Button*/ -->
<div class="collapse" id="IdHiddenDiv3">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
<!-- /*Submit Button*/ -->
</form> 
</div>
<!-- /*Details*/ -->
<!-- /************************************People************************************/ -->
<div class="col-lg-4">
<!-- Line -->
<h6>People</h6>
<!-- Line -->
<label for="StatusDdl" class="mr-sm-2">Reporter</label>
<label for="exampleSelect1" class="mr-sm-2">Assigned to</label>
</div>
</div>
<!-- /*People*/ -->
<!-- /***********************************Description**************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Description</h6>
<!-- Line -->
<form name="EditDescription" id="EditDescription" method="post" action="EditDescriptionAction.php" enctype="multipart/form-data">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv1" aria-expanded="false" aria-controls="IdHiddenDiv1">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<!-- Hidden field for table insertion -->
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<input type="hidden" name="TaskId" id ="TaskId" value=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
 >
<textarea class="form-control" rows="10" name="DescriptionTextArea" id="DescriptionTextArea" ><?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskDescription']; ?>
 </textarea>
<?php endfor; endif; ?>
<!-- Hidden field for table insertion -->
<div class="collapse" id="IdHiddenDiv1">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
</form>
</div>
<div class="col-lg-4">
<!-- Line -->
<h6>Dates</h6>
<!-- Line -->
</div>
</div>
<!-- /*Description*/ -->
<!-- /************************************Attachments************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Attachments</h6>
<!-- Line -->
<!-- Upload glyph-->
<p>Upload file:
<a href="#">
<span class="glyphicon glyphicon-upload"></span>
</a>
</p>
<!-- Upload glyph-->
</div>
</div>
<!-- /*Attachments*/ -->
<!-- /************************************Sub tasks************************************/ -->


<div class="row">
<div class="col-lg-8">
<!-- Line -->
<h6>Sub-tasks</h6>
<!-- Line -->

<!-- Sub-tasks table -->


<table class="table table-hover">
<tbody>
   <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items3']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
    <tr>
	  <td>$i</td>
	   <td><a href="SubTaskDetail.php?IssueId=<?php echo $this->_tpl_vars['items3'][$this->_sections['i']['index']]['SubTaskId']; ?>
"><?php echo $this->_tpl_vars['items3'][$this->_sections['i']['index']]['SubTaskSummary']; ?>
</a></td>
	  <td><?php echo $this->_tpl_vars['items3'][$this->_sections['i']['index']]['SubStatus']; ?>
</td>
	 
     </tr>
	 
	 <?php endfor; endif; ?> 
       
  </tbody>
</table>

<!-- Sub-tasks table -->



</div>
</div>



<!-- /*Sub tasks*/ -->
<!-- /************************************************Comments************************************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Comments</h6>
<!-- Line -->
<!-- Display below variable if there is no comment -->
<?php echo $this->_tpl_vars['NoDataVar']; ?>

<!-- Display below variable if there is no comment -->
<!-- Comment form start-->
<form name="CommmentForm" id="CommentForm" method="post" action="InsertCommentAction.php">
<!-- /*Hidden control for table insertion*/ -->
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<input type="hidden" name="TaskId" id ="TaskId" value=<?php echo $this->_tpl_vars['items'][$this->_sections['i']['index']]['TaskId']; ?>
 >
<?php endfor; endif; ?>
<!-- /*Hidden control for table insertion*/ -->
<ul class="list-unstyled">
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['items1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<li class="media">
<img class="d-flex mr-3" data-src="holder.js/64x64" alt="VS" src="" data-holder-rendered="true" style="width: 40px; height: 40px;">
<div class="media-body">

<a href="#"><?php echo $this->_tpl_vars['items1'][$this->_sections['i']['index']]['UserID']; ?>
</a> added a comment on <a href="#"><?php echo $this->_tpl_vars['items1'][$this->_sections['i']['index']]['CommentTime']; ?>
</a>
<p><?php echo $this->_tpl_vars['items1'][$this->_sections['i']['index']]['CommentValue']; ?>
</p>
<hr>
<?php endfor; endif; ?>
</div>
</li>
</ul>
</div>
<div class="col-lg-8" name="DivForComment">
<div class="collapse" id="IdHiddenDiv4">
<div class="form-group">
<label for="comment">Add comment:</label>
<textarea class="form-control" rows="4" name="CommentTextArea" id="CommentTextArea" ></textarea>
<hr>
<input type="submit" value="Add" class="btn btn-primary" >
</div>
</div>
</div>
<div class="col-lg-8" name="DivForCommentbutton">
<button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#IdHiddenDiv4" aria-expanded="false" aria-controls="IdHiddenDiv4">
<span class="glyphicon glyphicon-comment"></span>Comment
</button>
</div>
</form>
<!-- Comment form end-->

</div>

<!-- /*Comments*/ -->
</div>
</body>
</html>




  