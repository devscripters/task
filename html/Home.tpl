	<!DOCTYPE html>
	<html lang="en">
<!-- /*MODAL*/ -->

<div class="container-fluid">
<hr>
<!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-default"  data-toggle="modal" data-target="#myModal">Add new task</button>
<hr>
  <!-- Modal -->
  
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form name="FormForModal" method="post" action="TaskCreationAction.php">
 <input class="form-control" type="text" name="TaskSummary" id="TaskSummary">       
	   </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
		</form>
      </div>
    </div>
  </div>
</div>
  
  
  

<!-- Modal -->
<!-- /*TABLE*/ -->
<div class="container-fluid">
<table class="table table-hover">
<thead></thead>
<tr>
<th>Task Number</th>
<th>Task Summary</th>
<th>Planned For</th>
<th>Status</th>
</tr>
<tbody>
    {section name=i loop=$items start=0 step=1}
      <tr>
        <td><a href="TaskDetail.php?TaskId={$items[i].TaskId}">{$items[i].TaskId}</a></td>
        <td>{$items[i].TaskSummary}</td>
		 <td>{$items[i].PlannedFor}</td>
        <td>{$items[i].Status}</td>
       </tr>
       {/section }
      </tbody>
</table>

</div>


</body>
</html>




