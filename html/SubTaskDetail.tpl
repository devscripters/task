
<!DOCTYPE html>
<html lang="en">

<!-- /*ICONS*/ -->
<link rel='stylesheet' href='css/bootstrap.min.css' />
<!-- /*ICONS*/ -->

<!-- /*Horizontal rule*/ -->
<hr>
<!-- /*Horizontal rule*/ -->

<!-- /***************************************************************Back button*********************************/ -->
<div class="container-fluid">
{section name=i loop=$items start=0 step=1}
<a href="TaskDetail.php?TaskId={$items[i].TaskId}" class="btn btn-secondary" role="button" aria-pressed="true">
<span class="glyphicon glyphicon-arrow-left"></span>
</a>
{/section}
</a>
</div>
<!-- /*Back button*/ -->

<!-- /***************************************************Heading*******************************************/ -->
<div class="container-fluid">
<div class="row">
<div class="col-lg-10">
{section name=i loop=$items start=0 step=1}
<form name="EditSummary" id="EditSummary" method="post" action="EditSubTaskSummaryAction.php" enctype="multipart/form-data">
<!-- /*Hidden control for table insertion*/ -->
<input type="hidden" name="IssueId" id ="IssueId" value={$items[i].SubTaskId} >
<!-- /*Hidden control for table insertion*/ -->
<div class="container-fluid-fluid" id="header">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv2" aria-expanded="false" aria-controls="IdHiddenDiv2" >
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<p>
<textarea class="form-control" rows="1" name="Summary" id="Summary" >{$items[i].SubTaskSummary} </textarea>
{/section}
 </p>
<div class="collapse" id="IdHiddenDiv2">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</form>
</div>
</div>
</div>
</div>


<!-- /*Options*/ -->

<div class="col-lg-2">
<div class="dropdown">
<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-option-horizontal"></span>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
<a class="dropdown-item" href="#">Action1</a>
<a class="dropdown-item" href="#">Action2</a>
</div>
</div>
</div>

<!-- /*Options*/ -->

</div>
<!-- /*Heading*/ -->


<!-- /*****************************Details********************************************************************/ -->
<div class="row">
<div class="col-lg-8">
<!-- Line -->
<h6>Details</h6>
<!-- Line -->

<form  name="edit" id="edit "method="post" action="EditSubTaskDetailsAction.php">

<!-- /*Edit Button*/ -->
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv3" aria-expanded="false" aria-controls="IdHiddenDiv3">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<!-- /*Edit Button*/ -->

<!-- /*Hidden control for table insertion*/ -->
{section name=i loop=$items start=0 step=1}
<input type="hidden" name="IssueId" id ="IssueId" value={$items[i].SubTaskId} >
{/section}
<!-- /*Hidden control for table insertion*/ -->

<!-- /*Status*/ -->
<div class="form-group">
<label for="StatusDdl" class="mr-sm-2">Status</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="StatusDdl" name="StatusDdl">
{section name=i loop=$items start=0 step=1}
<option selected>{$items[i].SubStatus}</option>
{/section}
<option>OPEN</option>
<option>CLOSE</option>
<option>HOLD</option>
<option>FIXED</option> 
<option>IN PROGRESS</option>  
</select>
</div>
<!-- /*Status*/ -->

<!-- /*Project*/ -->


<div class="form-group">
<label for="exampleSelect1" class="mr-sm-2">Project</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="IsssueProjectName" name="IsssueProjectName">
{section name=i loop=$items start=0 step=1}
<!-- /*Please update with SubTaskProjectName*/ -->
<option >{$items[i].SubTaskProjectName}</option>
{/section}
{section name=i loop=$items2 start=0 step=1}
<option >{$items2[i].ProjectName}</option>
{/section}
</select>
</div>

<!-- /*Project*/ -->


<!-- /*Submit Button*/ -->
<div class="collapse" id="IdHiddenDiv3">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
<!-- /*Submit Button*/ -->

</form> 
</div>



<!-- /*Details*/ -->



<!-- /********************************People******************************/ -->
  
<div class="col-lg-4">
<!-- Line -->
<h6>People</h6>
<!-- Line -->

<label for="StatusDdl" class="mr-sm-2">Reporter</label>

<label for="exampleSelect1" class="mr-sm-2">Assigned to</label>

</div>



</div>

<!-- /*People*/ -->






<!-- /*************************************Description***********************************/ -->

<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Description</h6>
<!-- Line -->
<form name="EditDescription" id="EditDescription" method="post" action="EditSubTaskDescriptionAction.php">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv1" aria-expanded="false" aria-controls="IdHiddenDiv1">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
{section name=i loop=$items start=0 step=1}
<!-- /*Hidden control for table insertion*/ -->
<input type="hidden" name="IssueId" id ="IssueId" value={$items[i].SubTaskId} >
<!-- /*Hidden control for table insertion*/ -->
{/section}

{section name=i loop=$items start=0 step=1}
<textarea class="form-control" rows="10" name="DescriptionTextArea" id="DescriptionTextArea" >{$items[i].SubTaskDescription} </textarea>
{/section}
<div class="collapse" id="IdHiddenDiv1">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
</form>
</div>

<div class="col-lg-4">
<!-- Line -->
<h6>Dates</h6>
<!-- Line -->

</div>


</div>

<!-- /*Description*/ -->



<!-- /****************************************************Attachments******************************/ -->

<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Attachments</h6>
<!-- Line -->
Place holder
</div>
</div>

<!-- /*Attachments*/ -->

<!-- /************************************************Comments************************************************************/ -->

<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Comments</h6>
<!-- Line -->

<!-- Display below variable if there is no comment -->
{$NoDataVar}
<!-- Display below variable if there is no comment -->


<!-- Comment form start-->
<form name="CommmentForm" id="CommentForm" method="post" action="InsertSubTaskCommentAction.php">
<!-- /*Hidden control for table insertion*/ -->
{section name=i loop=$items start=0 step=1}
<input type="hidden" name="IssueId" id ="IssueId" value={$items[i].SubTaskId} >
{/section}
<!-- /*Hidden control for table insertion*/ -->
<ul class="list-unstyled">
{section name=i loop=$items1 start=0 step=1}
<li class="media">
<img class="d-flex mr-3" data-src="holder.js/64x64" alt="VS" src="" data-holder-rendered="true" style="width: 40px; height: 40px;">
<div class="media-body">

<a href="#">{$items1[i].UserID}</a> added a comment on <a href="#">{$items1[i].CommentTime}</a>
<p>{$items1[i].CommentValue}</p>
<hr>
{/section}
</div>
</li>
</ul>
</div>
<div class="col-lg-8" name="DivForComment">
<div class="collapse" id="IdHiddenDiv4">
<div class="form-group">
<label for="comment">Add comment:</label>
<textarea class="form-control" rows="4" name="CommentTextArea" id="CommentTextArea" ></textarea>
<hr>
<input type="submit" value="Add" class="btn btn-primary" >
</div>
</div>
</div>
<div class="col-lg-8" name="DivForCommentbutton">
<button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#IdHiddenDiv4" aria-expanded="false" aria-controls="IdHiddenDiv4">
<span class="glyphicon glyphicon-comment"></span> Comment
</button>
</div>
</form>
<!-- Comment form end-->

</div>

<!-- /*Comments*/ -->
</div>
</body>
</html>




  