
<!DOCTYPE html>
<html lang="en">
<!-- /*ICONS*/ -->
<link rel='stylesheet' href='css/bootstrap.min.css' />
<!-- /*ICONS*/ -->
<!-- /*************************************Heading************************************/ -->
<hr>
<!-- /************************************Back button************************************/ -->
<div class="container-fluid">
<a href="Home.php" class="btn btn-secondary" role="button" aria-pressed="true">
<span class="glyphicon glyphicon-arrow-left"></span>
</a>
</div>
<!-- /*Back button*/ -->
<div class="container-fluid">
<div class="row">
<div class="col-lg-10">
{section name=i loop=$items start=0 step=1}
<form name="EditSummary" id="EditSummary" method="post" action="EditSummaryAction.php" enctype="multipart/form-data">
<input type="hidden" name="TaskId" id ="TaskId" value={$items[i].TaskId}>
<div class="container-fluid-fluid" id="header">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv2" aria-expanded="false" aria-controls="IdHiddenDiv2" >
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<p>
<textarea class="form-control" rows="1" name="Summary" id="Summary" >{$items[i].TaskSummary} </textarea>
{/section}
 </p>
<div class="collapse" id="IdHiddenDiv2">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</form>
</div>
</div>
</div>
</div>
<!-- /************************************Options************************************/ -->
<div class="col-lg-2">
<div class="dropdown">
<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-option-horizontal"></span>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
{section name=i loop=$items start=0 step=1}
<a class="dropdown-item" href="CreateSubTaskAction.php?TaskId={$items[i].TaskId}">Create sub-task</a>
{/section}
<a class="dropdown-item" href="#">Another action</a>
<a class="dropdown-item" href="#">Something else here</a>
</div>
</div>
</div>
<!-- /*Options*/ -->
</div>
<!-- /*Heading*/ -->
<!-- /************************************Details************************************/ -->
<div class="row">
<div class="col-lg-8">
<!-- Line -->
<h6>Details</h6>
<!-- Line -->
<form name="edit" id="edit "method="post" action="EditStatusAction.php" enctype="multipart/form-data">
<!-- /*Edit Button*/ -->
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv3" aria-expanded="false" aria-controls="IdHiddenDiv3">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<!-- /*Edit Button*/ -->
<!-- /*Hidden*/ -->
{section name=i loop=$items start=0 step=1}
<input type="hidden" name="TaskId" id ="TaskId" value={$items[i].TaskId} >
{/section}
<!-- /*Hidden*/ -->
<!-- /*Status*/ -->
<div class="form-group">
<label for="StatusDdl" class="mr-sm-2">Status</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="StatusDdl" name="StatusDdl">
{section name=i loop=$items start=0 step=1}
<option selected>{$items[i].Status}</option>
{/section}
<option>OPEN</option>
<option>CLOSE</option>
<option>HOLD</option>
<option>FIXED</option> 
<option>IN PROGRESS</option>  
</select>
</div>
<!-- /*Status*/ -->
<!-- /*PlannedFor*/ -->
<div class="form-group">
<label for="PlannedForLabel" class="mr-sm-2">Planned For</label>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="PlannedDdl" name="PlannedDdl">
{section name=i loop=$items start=0 step=1}
<option selected>{$items[i].PlannedFor}</option>
{/section}
<option>Today</option>
<option>Daily</option>
<option>Weekly</option>
<option>Monthly</option>
<option>Future</option>
</select>
</div>
<!-- /*PlannedFor*/ -->
<!-- /*Submit Button*/ -->
<div class="collapse" id="IdHiddenDiv3">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
<!-- /*Submit Button*/ -->
</form> 
</div>
<!-- /*Details*/ -->
<!-- /************************************People************************************/ -->
<div class="col-lg-4">
<!-- Line -->
<h6>People</h6>
<!-- Line -->
<label for="StatusDdl" class="mr-sm-2">Reporter</label>
<label for="exampleSelect1" class="mr-sm-2">Assigned to</label>
</div>
</div>
<!-- /*People*/ -->
<!-- /***********************************Description**************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Description</h6>
<!-- Line -->
<form name="EditDescription" id="EditDescription" method="post" action="EditDescriptionAction.php" enctype="multipart/form-data">
<button id="btn-edit" class="btn btn-secondary type="button" data-toggle="collapse" data-target="#IdHiddenDiv1" aria-expanded="false" aria-controls="IdHiddenDiv1">
<span class="glyphicon glyphicon-pencil"></span> 
</button>
<!-- Hidden field for table insertion -->
{section name=i loop=$items start=0 step=1}
<input type="hidden" name="TaskId" id ="TaskId" value={$items[i].TaskId} >
<textarea class="form-control" rows="10" name="DescriptionTextArea" id="DescriptionTextArea" >{$items[i].TaskDescription} </textarea>
{/section}
<!-- Hidden field for table insertion -->
<div class="collapse" id="IdHiddenDiv1">
<div class="form-group">
<button type="submit" class="btn" name="btn-edit"><span class="glyphicon glyphicon-ok"></span></button>
</div>
</div>
</form>
</div>
<div class="col-lg-4">
<!-- Line -->
<h6>Dates</h6>
<!-- Line -->
</div>
</div>
<!-- /*Description*/ -->
<!-- /************************************Attachments************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Attachments</h6>
<!-- Line -->
<!-- Upload glyph-->
<p>Upload file:
<a href="#">
<span class="glyphicon glyphicon-upload"></span>
</a>
</p>
<!-- Upload glyph-->
</div>
</div>
<!-- /*Attachments*/ -->
<!-- /************************************Sub tasks************************************/ -->


<div class="row">
<div class="col-lg-8">
<!-- Line -->
<h6>Sub-tasks</h6>
<!-- Line -->

<!-- Sub-tasks table -->


<table class="table table-hover">
<tbody>
   {section name=i loop=$items3 start=0 step=1}
    <tr>
	  <td>$i</td>
	   <td><a href="SubTaskDetail.php?IssueId={$items3[i].SubTaskId}">{$items3[i].SubTaskSummary}</a></td>
	  <td>{$items3[i].SubStatus}</td>
	 
     </tr>
	 
	 {/section} 
       
  </tbody>
</table>

<!-- Sub-tasks table -->



</div>
</div>



<!-- /*Sub tasks*/ -->
<!-- /************************************************Comments************************************************************/ -->
<div class="row">
<div class="col-lg-8" name="DivForDescription">
<!-- Line -->
<h6>Comments</h6>
<!-- Line -->
<!-- Display below variable if there is no comment -->
{$NoDataVar}
<!-- Display below variable if there is no comment -->
<!-- Comment form start-->
<form name="CommmentForm" id="CommentForm" method="post" action="InsertCommentAction.php">
<!-- /*Hidden control for table insertion*/ -->
{section name=i loop=$items start=0 step=1}
<input type="hidden" name="TaskId" id ="TaskId" value={$items[i].TaskId} >
{/section}
<!-- /*Hidden control for table insertion*/ -->
<ul class="list-unstyled">
{section name=i loop=$items1 start=0 step=1}
<li class="media">
<img class="d-flex mr-3" data-src="holder.js/64x64" alt="VS" src="" data-holder-rendered="true" style="width: 40px; height: 40px;">
<div class="media-body">

<a href="#">{$items1[i].UserID}</a> added a comment on <a href="#">{$items1[i].CommentTime}</a>
<p>{$items1[i].CommentValue}</p>
<hr>
{/section}
</div>
</li>
</ul>
</div>
<div class="col-lg-8" name="DivForComment">
<div class="collapse" id="IdHiddenDiv4">
<div class="form-group">
<label for="comment">Add comment:</label>
<textarea class="form-control" rows="4" name="CommentTextArea" id="CommentTextArea" ></textarea>
<hr>
<input type="submit" value="Add" class="btn btn-primary" >
</div>
</div>
</div>
<div class="col-lg-8" name="DivForCommentbutton">
<button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#IdHiddenDiv4" aria-expanded="false" aria-controls="IdHiddenDiv4">
<span class="glyphicon glyphicon-comment"></span>Comment
</button>
</div>
</form>
<!-- Comment form end-->

</div>

<!-- /*Comments*/ -->
</div>
</body>
</html>




  